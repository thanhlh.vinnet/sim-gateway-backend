package vinnet.sim.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import vinnet.sim.domain.response.DataRes;
import vinnet.sim.exceptions.BadRequestException;
import vinnet.sim.helper.Constant;
import vinnet.sim.helper.Helper;
import vinnet.sim.model.SimProduct;

import java.util.ArrayList;
import java.util.List;

@Service
public class PhoneService {

    public DataRes importSim(MultipartFile excelFile){
        List<SimProduct> simProducts = new ArrayList<>();

        try{
            if (Helper.hasExcelFormat(excelFile)) {
                //import file excel chua sanh sach so dien thoai vao db
                simProducts = Helper.readExcelToPhoneNumber(excelFile);
            }
        }catch(BadRequestException badRequestException){
            throw new BadRequestException(Constant.CELL_TYPE_EXCEL_WAS_WRONG);
        }

        return new DataRes(simProducts);
    }
}
