package vinnet.sim.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Getter
public enum DomainCode {
  SUCCESS("000", "Success", HttpStatus.OK.value()),
  PHONE_INVALID("001", "Phone number is invalid", HttpStatus.BAD_REQUEST.value()),
  ;
  private final String code;
  private final String message;
  private final int status;
}
