package vinnet.sim.helper;

import lombok.Data;

@Data
public class Constant {
    public static final String UPLOAD_FILE_SUCCESS = "UPLOAD_FILE_SUCCESS";
    public static final String CELL_TYPE_EXCEL_WAS_WRONG = "CELL_TYPE_EXCEL_WAS_WRONG"; //Kiểu dữ liệu sai định dạng
    public static final String LACK_OF_VARIABLE_IN_EXCEL = "LACK_OF_VARIABLE_IN_EXCEL";


}
