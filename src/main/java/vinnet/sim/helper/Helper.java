package vinnet.sim.helper;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import vinnet.sim.exceptions.BadRequestException;
import vinnet.sim.model.SimProduct;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Slf4j
@Component
public class Helper {
    protected static final List<String> EXCEL_TYPE = Arrays.asList("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/wps-office.xls", "application/wps-office.xlsx");

    public static boolean hasExcelFormat(MultipartFile file) {
        return EXCEL_TYPE.contains(file.getContentType());
    }

    public static List<SimProduct> readExcelToPhoneNumber(MultipartFile file) {
        try {
            String[] types = {"application/vnd.ms-excel", "application/wps-office.xls", "application/xls"};
            Workbook workbook = Arrays.asList(types).contains(file.getContentType()) ?
                    new HSSFWorkbook(file.getInputStream()) : new XSSFWorkbook(file.getInputStream());
            Sheet sheet = workbook.getSheetAt(0);
            List<SimProduct> simProducts = new ArrayList<>();

            int totalRow = sheet.getLastRowNum();
            for (int i = 1; i <= totalRow; i++) {
                Row currentRow = sheet.getRow(i);
                if (currentRow == null || isRowEmpty(currentRow)) continue;

                SimProduct simProduct = new SimProduct();

                // Đọc giá trị từ cột "id", "phone_number", "price", và "prepaid"
                simProduct.setNumber(currentRow.getCell(1).getStringCellValue());
                simProduct.setPrice(currentRow.getCell(2).getNumericCellValue());
                simProduct.setIsPrepaid(currentRow.getCell(3).getBooleanCellValue());

                simProducts.add(simProduct);
            }

            workbook.close();
            return simProducts;
        } catch (IOException e) {
            e.printStackTrace();  // Thay thế bằng xử lý ngoại lệ thích hợp
            throw new BadRequestException(Constant.CELL_TYPE_EXCEL_WAS_WRONG);
        }
    }

    private static boolean isRowEmpty(Row row) {
        for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
            Cell cell = row.getCell(c);
            if (cell != null && cell.getCellType() != CellType.BLANK) {
                return false;
            }
        }
        return true;
    }

}
