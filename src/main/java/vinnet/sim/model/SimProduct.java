package vinnet.sim.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@Entity
@Table(name = "sim_product")
public class SimProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "number")
    private String number;

    @Column(name = "is_locked")
    private Boolean isLocked;

    @Column(name = "is_prepaid")
    private Boolean isPrepaid;

    @Column(name = "price")
    private Double price;


    @Column(name = "created_at")
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "sim_product_category",
            joinColumns = @JoinColumn(name = "sim_product_id"),
            inverseJoinColumns = @JoinColumn(name = "sim_category_id"))
    private Set<SimCategory> simCategories = new HashSet<>();
}
