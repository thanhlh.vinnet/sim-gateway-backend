package vinnet.sim.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FOUND)
public class ResourceExitsException extends BaseException {
  private static final long serialVersionUID = 1L;

  public ResourceExitsException(String message) {
    super(message);
  }
}
