package vinnet.sim.exceptions;

public class BadRequestException extends BaseException{
    // code 400
    private static final long serialVersionUID = 1L;

    public BadRequestException(String message) {
        super(message);
    }
}
