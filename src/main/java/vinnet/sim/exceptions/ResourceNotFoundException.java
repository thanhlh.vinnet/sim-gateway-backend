package vinnet.sim.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends BaseException{
    // ban exception khi check id sai thong tin
    private static final long serialVersionUID = 1L;

    public ResourceNotFoundException() {
        super("resource not");
    }

    public ResourceNotFoundException(String message) {
        super(message);
    }
}
