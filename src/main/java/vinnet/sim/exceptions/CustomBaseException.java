package vinnet.sim.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import vinnet.sim.enums.DomainCode;

@Getter
@Setter
public class CustomBaseException extends RuntimeException{
  private final DomainCode code;
  private transient Object[] args;
  private HttpStatus status;

  public CustomBaseException(String message, DomainCode code) {
    super(message);
    this.code = code;
  }
  public CustomBaseException(DomainCode code) {
    super();
    this.code = code;
  }
}
