package vinnet.sim.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;
import vinnet.sim.service.PhoneService;

@RestController
@Validated
public class BaseController {
    @Autowired
    PhoneService phoneService;

}
