package vinnet.sim.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/api/sim")
public class PhoneController extends BaseController{
    //inport số điện thoại
    @PostMapping()
    public ResponseEntity<Object> createCampaign(@ModelAttribute MultipartFile excelFile) {
        return ResponseEntity.ok(phoneService.importSim(excelFile));
    }
}
