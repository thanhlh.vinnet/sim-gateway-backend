package vinnet.sim.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomPage <T> {
    List<T> data;
    CustomPageable pageable;

    public CustomPage(Page<T> page) {
        this.data = page.getContent().isEmpty() ? new ArrayList<>() : page.getContent();
        this.pageable = new CustomPageable(page.getPageable().getPageNumber(),
                page.getPageable().getPageSize(),
                page.getTotalElements(),
                page.getTotalPages());
    }

    public CustomPage(List<T> data, Pageable page) {
        this.data = data;
        this.pageable = new CustomPageable(page.getPageNumber(),
                page.getPageSize(),
                0,
                0);
    }
}
