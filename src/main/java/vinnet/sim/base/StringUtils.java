package vinnet.sim.base;

import java.text.SimpleDateFormat;
import java.util.Date;


public class StringUtils {
  private StringUtils() {
    throw new IllegalStateException("Utility class");
  }

  public static Date convertStringToDate(String inputDateString) {
    try {
      if (StringUtils.isNullOrEmpty(inputDateString)) {
        return null;
      } else if (inputDateString.length() == 8) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd"); // pattern = "yyyyMMdd"
        dateFormat.setLenient(false);
        return dateFormat.parse(inputDateString);
      } else if (inputDateString.length() == 10) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHH"); // pattern = "yyyyMMddHH"
        dateFormat.setLenient(false);
        return dateFormat.parse(inputDateString);
      } else if (inputDateString.length() == 12) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm"); // pattern = "yyyyMMddHHmm"
        dateFormat.setLenient(false);
        return dateFormat.parse(inputDateString);
      } else if (inputDateString.length() == 14) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss"); // pattern = "yyyyMMddHHmmss"
        dateFormat.setLenient(false);
        return dateFormat.parse(inputDateString);
      }
      return null;
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  public static boolean isNullOrEmpty(String s) {
    if (s == null || s.isEmpty()) {
      return true;
    }
    s = s.trim();
    if (s.isEmpty()) return true;
    return false;
  }

}
