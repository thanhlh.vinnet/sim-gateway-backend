package vinnet.sim.base;

import lombok.Data;

@Data
public class CustomPageable {
    int pageNumber;
    int pageSize;
    long totalElements;
    long totalPages;

    public CustomPageable(int pageNumber, int pageSize, long totalElements, long totalPages) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.totalElements = totalElements;
        this.totalPages = totalPages;
    }
}
