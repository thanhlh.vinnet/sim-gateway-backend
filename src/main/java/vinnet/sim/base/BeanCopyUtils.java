package vinnet.sim.base;

import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;

public class BeanCopyUtils {
  private BeanCopyUtils() {
    throw new IllegalStateException("BeanCopyUtils");
  }

  public static void copyProperties(final Object dest, final Object orig) {
    try {
      BeanUtils.copyProperties(dest, orig);
    } catch (IllegalAccessException | InvocationTargetException e) {
      throw new IllegalArgumentException(e);
    }
  }
}
