package vinnet.sim.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vinnet.sim.model.SimProduct;

public interface SimProductRepository extends JpaRepository<SimProduct, Long> {
}
