package vinnet.sim.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vinnet.sim.model.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
}
