package vinnet.sim.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vinnet.sim.model.SimCategory;

public interface SimCategoryRepository extends JpaRepository<SimCategory, Long> {
}
