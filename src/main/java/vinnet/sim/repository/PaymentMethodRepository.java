package vinnet.sim.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vinnet.sim.model.PaymentMethod;

public interface PaymentMethodRepository extends JpaRepository<PaymentMethod, Long> {
}
